<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Tests the archive log store Privacy API implementation.
 *
 * @package     logstore_archive
 * @copyright   University of Nottingham, 2018
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace logstore_archive\privacy;

use context_course;
use core_privacy\local\request\contextlist;
use core_privacy\local\request\writer;

defined('MOODLE_INTERNAL') || die();

require_once(dirname(__DIR__) . '/fixtures/event.php');

/**
 * Tests the archive log store privacy provider class.
 *
 * @package     logstore_archive
 * @copyright   University of Nottingham, 2018
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group logstore_archive
 * @group uon
 */
class provider_test extends \core_privacy\tests\provider_testcase {
    use \logstore_archive\local\testing\local_table;

    /**
     * Get the contextlist for a user.
     *
     * @param object $user The user.
     * @return contextlist
     */
    protected function get_contextlist_for_user($user) {
        $contextlist = new contextlist();
        provider::add_contexts_for_userid($contextlist, $user->id);
        return $contextlist->get_contexts();
    }

    /**
     * Test that when a user has no logs that they no contexts are found.
     */
    public function test_user_with_no_logs() {
        global $DB;
        $this->resetAfterTest();
        // Create some users.
        $user = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        // Create a log record for the other user.
        $ctx = context_course::instance(SITEID);
        $record = (object) array(
            'eventname' => '\logstore_archive\event\unittest_executed',
            'component' => 'logstore_archive',
            'target' => 'unittest',
            'action' => 'executed',
            'crud' => 'u',
            'edulevel' => (string)\core\event\base::LEVEL_PARTICIPATING,
            'contextid' => "$ctx->id",
            'contextlevel' => "$ctx->contextlevel",
            'contextinstanceid' => "$ctx->instanceid",
            'userid' => "$otheruser->id",
            'timecreated' => time(),
            'objecttable' => null,
            'objectid' => null,
            'courseid' => (string)SITEID,
            'relateduserid' => null,
            'anonymous' => '0',
            'other' => '',
        );
        $DB->insert_record(self::$tablename, $record);
        // Test no contexts are retrieved.
        $this->assertEquals([], $this->get_contextlist_for_user($user));
    }

    /**
     * Test that we get data for the user when they have a log entry.
     */
    public function test_user_with_log() {
        global $DB;
        $this->resetAfterTest();
        // Create some users.
        $user = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        // Create a log record for the users.
        $context = context_course::instance(SITEID);
        $record = (object) array(
            'eventname' => '\logstore_archive\event\unittest_executed',
            'component' => 'logstore_archive',
            'target' => 'unittest',
            'action' => 'executed',
            'crud' => 'u',
            'edulevel' => (string)\core\event\base::LEVEL_PARTICIPATING,
            'contextid' => "$context->id",
            'contextlevel' => "$context->contextlevel",
            'contextinstanceid' => "$context->instanceid",
            'userid' => "$user->id",
            'timecreated' => time(),
            'objecttable' => null,
            'objectid' => null,
            'courseid' => (string)SITEID,
            'relateduserid' => null,
            'anonymous' => '0',
            'other' => '',
            'realuserid' => null,
        );
        $DB->insert_record(self::$tablename, $record);
        $record->userid = "$otheruser->id";
        $DB->insert_record(self::$tablename, $record);
        $record->relateduserid = "$user->id";
        $DB->insert_record(self::$tablename, $record);
        $record->realuserid = "$user->id";
        $record->relateduserid = null;
        $DB->insert_record(self::$tablename, $record);
        // Test a context is retrieved.
        $this->assertEquals([$context], $this->get_contextlist_for_user($user));
        // Test export.
        $path = [get_string('privacy:path:logs', 'tool_log'), get_string('pluginname', 'logstore_archive')];
        $approvedcontextlist = new \core_privacy\tests\request\approved_contextlist(
            \core_user::get_user($user->id),
            'logstore_archive',
            [$context->id]
        );
        provider::export_user_data($approvedcontextlist);
        $data = writer::with_context($context)->get_data($path);
        $this->assertCount(3, $data->logs);
    }

    /**
     * Test that we delete the resolved logs for the user context.
     */
    public function test_delete_data_for_all_users_in_context() {
        global $DB;
        $this->resetAfterTest();
        // Create some users.
        $user = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        $context = context_course::instance($course->id);
        $othercourse = self::getDataGenerator()->create_course();
        $othercontext = context_course::instance($othercourse->id);
        // Create a log record for the users.
        $record = (object) array(
            'eventname' => '\logstore_archive\event\unittest_executed',
            'component' => 'logstore_archive',
            'target' => 'unittest',
            'action' => 'executed',
            'crud' => 'u',
            'edulevel' => (string)\core\event\base::LEVEL_PARTICIPATING,
            'contextid' => "$context->id",
            'contextlevel' => "$context->contextlevel",
            'contextinstanceid' => "$context->instanceid",
            'userid' => "$user->id",
            'timecreated' => time(),
            'objecttable' => null,
            'objectid' => null,
            'courseid' => "$course->id",
            'relateduserid' => null,
            'anonymous' => '0',
            'other' => '',
            'realuserid' => null,
        );
        $DB->insert_record(self::$tablename, $record);
        $record->userid = "$otheruser->id";
        $DB->insert_record(self::$tablename, $record);
        $record->contextid = "$othercontext->id";
        $record->courseid = "$othercourse->id";
        $DB->insert_record(self::$tablename, $record);
        // Make the call to delete.
        provider::delete_data_for_all_users_in_context($context);
        // Test the records in the context have been deleted.
        $this->assertEquals(0, $DB->count_records(self::$tablename, ['contextid' => $context->id]));
        // Test that records in other contexts have not been deleted.
        $this->assertEquals(1, $DB->count_records(self::$tablename));
    }

    /**
     * Test that we delete the resolved logs for the user.
     */
    public function test_delete_data_for_user() {
        global $DB;
        $this->resetAfterTest();
        // Create some users.
        $user = self::getDataGenerator()->create_user();
        $otheruser = self::getDataGenerator()->create_user();
        $course = self::getDataGenerator()->create_course();
        $context = context_course::instance($course->id);
        $othercourse = self::getDataGenerator()->create_course();
        $othercontext = context_course::instance($othercourse->id);
        // Create a log record for the users.
        $record = (object) array(
            'eventname' => '\logstore_archive\event\unittest_executed',
            'component' => 'logstore_archive',
            'target' => 'unittest',
            'action' => 'executed',
            'crud' => 'u',
            'edulevel' => (string)\core\event\base::LEVEL_PARTICIPATING,
            'contextid' => "$context->id",
            'contextlevel' => "$context->contextlevel",
            'contextinstanceid' => "$context->instanceid",
            'userid' => "$user->id",
            'timecreated' => time(),
            'objecttable' => null,
            'objectid' => null,
            'courseid' => "$course->id",
            'relateduserid' => null,
            'anonymous' => '0',
            'other' => '',
            'realuserid' => null,
        );
        $DB->insert_record(self::$tablename, $record);
        // Records that should not be deleted.
        $record->userid = "$otheruser->id";
        $DB->insert_record(self::$tablename, $record);
        $record->relateduserid = "$user->id";
        $DB->insert_record(self::$tablename, $record);
        $record->realuserid = "$user->id";
        $record->relateduserid = null;
        $DB->insert_record(self::$tablename, $record);
        $record->contextid = "$othercontext->id";
        $record->courseid = "$othercourse->id";
        $record->userid = "$user->id";
        $DB->insert_record(self::$tablename, $record);
        // Make the call to delete.
        $approvedcontextlist = new \core_privacy\tests\request\approved_contextlist(
            \core_user::get_user($user->id),
            'logstore_archive',
            [$context->id]
        );
        provider::delete_data_for_user($approvedcontextlist);
        // Test the records in the context have been deleted.
        $this->assertEquals(0, $DB->count_records(self::$tablename, ['userid' => $user->id, 'contextid' => $context->id]));
        // Test that records created by other users have not been deleted.
        $this->assertEquals(3, $DB->count_records(self::$tablename, ['contextid' => $context->id]));
        // Test that other contexts have not been affected.
        $this->assertEquals(4, $DB->count_records(self::$tablename));
    }
}
