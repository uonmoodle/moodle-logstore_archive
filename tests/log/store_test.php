<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Standard log archive store tests.
 *
 * @package    logstore_archive
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2015 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace logstore_archive\log;

use context_course;

defined('MOODLE_INTERNAL') || die();

require_once(dirname(__DIR__) . '/fixtures/event.php');

/**
 * Tests the log store class of logstore_archive.
 *
 * @package    logstore_archive
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2015 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group logstore_archive
 * @group uon
 */
class store_test extends \advanced_testcase {
    use \logstore_archive\local\testing\local_table;

    /**
     * Tests that the archive task moves events correctly.
     *
     * @covers \logstore_archive\task\archive_task::execute
     * @group logstore_archive
     * @group uon
     */
    public function test_archive_task() {
        global $DB;
        $this->resetAfterTest();
        $expectedoutput = '';
        // Create some records spread over various days.
        $ctx = context_course::instance(1);
        $record = (object) array(
            'edulevel' => 0,
            'contextid' => $ctx->id,
            'contextlevel' => $ctx->contextlevel,
            'contextinstanceid' => $ctx->instanceid,
            'userid' => 1,
            'timecreated' => time(),
        );

        $DB->insert_record('logstore_standard_log', $record);
        $record->timecreated -= 3600 * 24 * 30;
        $DB->insert_record('logstore_standard_log', $record);
        $record->timecreated -= 3600 * 24 * 30;
        $DB->insert_record('logstore_standard_log', $record);
        $record->timecreated -= 3600 * 24 * 30;
        $DB->insert_record('logstore_standard_log', $record);
        $this->assertEquals(4, $DB->count_records('logstore_standard_log'));
        $this->assertEquals(0, $DB->count_records(self::$tablename));

        // Remove all logs before "today".
        set_config('archiveafter', DAYSECS, 'logstore_archive');
        // Disable logstores.
        set_config('enabled_stores', '', 'tool_log');
        // Force a reset of the log manager otherwise this test may fail.
        get_log_manager(true);

        $expectedoutput .= "Standard logstore disabled.\n";
        $this->expectOutputString($expectedoutput);
        $clean = new \logstore_archive\task\archive_task();
        $clean->execute();

        $this->assertEquals(4, $DB->count_records('logstore_standard_log'));
        $this->assertEquals(0, $DB->count_records(self::$tablename));

        // Enable the standard logstore.
        set_config('enabled_stores', 'logstore_standard', 'tool_log');
        set_config('buffersize', 0, 'logstore_standard');
        set_config('logguests', 1, 'logstore_standard');
        // Force a reset of the log manager otherwise this test may fail.
        get_log_manager(true);

        $expectedoutput .= " Archived 3 log records from standard store to the standard log archive.\n";
        $clean = new \logstore_archive\task\archive_task();
        $clean->execute();

        $this->expectOutputString($expectedoutput);
        $this->assertEquals(1, $DB->count_records('logstore_standard_log'));
        $this->assertEquals(3, $DB->count_records(self::$tablename));
    }

    /**
     * Test that the standard archive log cleanup works correctly.
     *
     * @covers \logstore_archive\task\cleanup_task::execute
     * @group logstore_archive
     * @group uon
     */
    public function test_cleanup_task() {
        global $DB;
        $this->resetAfterTest();
        // Create some records spread over various days; test multiple iterations in cleanup.
        $ctx = context_course::instance(1);
        $record = (object) array(
            'edulevel' => 0,
            'contextid' => $ctx->id,
            'contextlevel' => $ctx->contextlevel,
            'contextinstanceid' => $ctx->instanceid,
            'userid' => 1,
            'timecreated' => time(),
        );
        $DB->insert_record(self::$tablename, $record);
        $record->timecreated -= 3600 * 24 * 30;
        $DB->insert_record(self::$tablename, $record);
        $record->timecreated -= 3600 * 24 * 30;
        $DB->insert_record(self::$tablename, $record);
        $record->timecreated -= 3600 * 24 * 30;
        $DB->insert_record(self::$tablename, $record);
        $this->assertEquals(4, $DB->count_records(self::$tablename));

        // Remove all logs before "today".
        set_config('archivelifetime', DAYSECS, 'logstore_archive');

        $this->expectOutputString(" Deleted old log records from standard archive store.\n");
        $clean = new \logstore_archive\task\cleanup_task();
        $clean->execute();

        $this->assertEquals(1, $DB->count_records(self::$tablename));
    }

    /**
     * Tests that the events returned by the plugin are correct.
     *
     * @global \moodle_database $DB
     * @covers \logstore_archive\log\store::get_events_select_count
     * @covers \logstore_archive\log\store::get_events_select
     * @group logstore_archive
     * @group uon
     */
    public function test_read() {
        global $DB;
        $this->resetAfterTest();
        // Create some records spread over various days; test multiple iterations in cleanup.
        $ctx = context_course::instance(SITEID);
        $record = (object) array(
            'eventname' => '\logstore_archive\event\unittest_executed',
            'component' => 'logstore_archive',
            'target' => 'unittest',
            'action' => 'executed',
            'crud' => 'u',
            'edulevel' => (string)\core\event\base::LEVEL_PARTICIPATING,
            'contextid' => "$ctx->id",
            'contextlevel' => "$ctx->contextlevel",
            'contextinstanceid' => "$ctx->instanceid",
            'userid' => '1',
            'timecreated' => time(),
            'objecttable' => null,
            'objectid' => null,
            'courseid' => null,
            'relateduserid' => null,
            'anonymous' => '0',
            'other' => json_encode([]),
        );
        $DB->insert_record(self::$tablename, $record);
        $record2 = clone($record);
        $record2->timecreated -= 3600 * 24 * 30;
        $DB->insert_record(self::$tablename, $record2);

        // Fix up the records to contain the values as an event should return them.
        $record->timecreated = (string)$record->timecreated;
        $record->other = array();
        $record2->timecreated = (string)$record2->timecreated;
        $record2->other = array();

        set_config('enabled_stores', 'logstore_archive', 'tool_log');
        $manager = get_log_manager(true);

        $stores = $manager->get_readers();
        $this->assertCount(1, $stores);
        $this->assertEquals(array('logstore_archive'), array_keys($stores));
        $store = $stores['logstore_archive'];
        $this->assertInstanceOf('logstore_archive\log\store', $store);
        $this->assertInstanceOf('core\log\sql_reader', $store);
        $this->assertFalse($store->is_logging());

        $this->assertTrue($store->get_events_select_exists('', []));
        $this->assertFalse($store->get_events_select_exists('userid = :userid', ['userid' => 200]));
        $this->assertSame(2, $store->get_events_select_count('', array()));
        // Is actually sorted by "timecreated ASC, id ASC".
        $events = $store->get_events_select('', array(), 'timecreated ASC', 0, 0);
        $this->assertCount(2, $events);
        $event1 = array_shift($events);
        $event2 = array_shift($events);

        $this->assertEquals((array)$record2, $event1->get_data());
        $this->assertEquals((array)$record, $event2->get_data());
    }

    /**
     * Tests that the events returned by the plugin are correct.
     *
     * @global \moodle_database $DB
     * @covers \logstore_archive\log\store::get_events_select_iterator
     * @covers \logstore_archive\log\store::get_log_event
     * @group logstore_archive
     * @group uon
     */
    public function test_read_traversable() {
        global $DB;
        $this->resetAfterTest();
        // Create some records spread over various days; test multiple iterations in cleanup.
        $ctx = context_course::instance(1);
        $record = (object) array(
            'eventname' => '\logstore_archive\event\unittest_executed',
            'component' => 'logstore_archive',
            'target' => 'unittest',
            'action' => 'executed',
            'crud' => 'u',
            'edulevel' => (string)\core\event\base::LEVEL_PARTICIPATING,
            'contextid' => "$ctx->id",
            'contextlevel' => "$ctx->contextlevel",
            'contextinstanceid' => "$ctx->instanceid",
            'userid' => '1',
            'timecreated' => time(),
            'objecttable' => null,
            'objectid' => null,
            'courseid' => null,
            'relateduserid' => null,
            'anonymous' => '0',
            'other' => serialize([]),
        );
        $DB->insert_record(self::$tablename, $record);
        $record2 = clone($record);
        $record2->timecreated -= 3600 * 24 * 30;
        $DB->insert_record(self::$tablename, $record2);

        // Fix up the records to contain the values as an event should return them.
        $record->timecreated = (string)$record->timecreated;
        $record->other = array();
        $record2->timecreated = (string)$record2->timecreated;
        $record2->other = array();

        set_config('enabled_stores', 'logstore_archive', 'tool_log');
        $manager = get_log_manager(true);

        $stores = $manager->get_readers();
        $this->assertCount(1, $stores);
        $this->assertEquals(array('logstore_archive'), array_keys($stores));
        $store = $stores['logstore_archive'];
        $this->assertInstanceOf('logstore_archive\log\store', $store);
        $this->assertInstanceOf('core\log\sql_reader', $store);
        $this->assertFalse($store->is_logging());

        $iterator = $store->get_events_select_iterator('', array(), '', 0, 500);
        $this->assertTrue($iterator->valid());
        $event1 = $iterator->current();
        $this->assertInstanceOf('\core\event\base', $event1);
        $this->assertEquals((array)$record, $event1->get_data());
        $iterator->next();
        $this->assertTrue($iterator->valid());
        $event2 = $iterator->current();
        $this->assertInstanceOf('\core\event\base', $event2);
        $this->assertEquals((array)$record2, $event2->get_data());
        $iterator->next();
        $this->assertFalse($iterator->valid());
        $iterator->close();
    }
}
