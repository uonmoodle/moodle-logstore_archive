<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Standard log archive upgrade script.
 *
 * @package    logstore_archive
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2019 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function xmldb_logstore_archive_upgrade($oldversion) {
    if ($oldversion < 2019011700) {
        // Convert config settings from days to seconds.
        $archiveafter = get_config('logstore_archive', 'archiveafter');
        if ($archiveafter !== false) {
            set_config('archiveafter', $archiveafter * DAYSECS, 'logstore_archive');
        }
        $archivelifetime = get_config('logstore_archive', 'archivelifetime');
        if ($archivelifetime !== false) {
            set_config('archivelifetime', $archivelifetime * DAYSECS, 'logstore_archive');
        }
        upgrade_plugin_savepoint(true, '2019011700', 'logstore', 'archive');
    }
    return true;
}
