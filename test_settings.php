<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Displays messages about the status of the configured database connection.
 *
 * This file is a minimally changed copy of the file from the core logstore_database plugin.
 *
 * @package    logstore_archive
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2015 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../../../../config.php');
require_once($CFG->dirroot . '/lib/adminlib.php');

require_login();
$context = context_system::instance();
require_capability('moodle/site:config', $context);
require_sesskey();

navigation_node::override_active_url(new moodle_url('/admin/settings.php', array('section' => 'logsettingdatabase')));
admin_externalpage_setup('logstorearchivetestsettings');

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('testingsettings', 'logstore_archive'));

// NOTE: this is not localised intentionally, admins are supposed to understand English at least a bit...

raise_memory_limit(MEMORY_HUGE);
$dbtable = get_config('logstore_archive', 'dbtable');
if (empty($dbtable)) {
    echo $OUTPUT->notification('External table not specified.', 'notifyproblem');
    die();
}

$dbdriver = get_config('logstore_archive', 'dbdriver');
list($dblibrary, $dbtype) = explode('/', $dbdriver);
if (!$db = \moodle_database::get_driver_instance($dbtype, $dblibrary, true)) {
    echo $OUTPUT->notification("Unknown driver $dblibrary/$dbtype", "notifyproblem");
    die();
}

$olddebug = $CFG->debug;
$olddisplay = ini_get('display_errors');
ini_set('display_errors', '1');
$CFG->debug = DEBUG_DEVELOPER;
error_reporting($CFG->debug);

$dboptions = array();
$dboptions['dbpersist'] = get_config('logstore_archive', 'dbpersist');
$dboptions['dbsocket'] = get_config('logstore_archive', 'dbsocket');
$dboptions['dbport'] = get_config('logstore_archive', 'dbport');
$dboptions['dbschema'] = get_config('logstore_archive', 'dbschema');
$dboptions['dbcollation'] = get_config('logstore_archive', 'dbcollation');

try {
    $db->connect(get_config('logstore_archive', 'dbhost'), get_config('logstore_archive', 'dbuser'),
        get_config('logstore_archive', 'dbpass'), get_config('logstore_archive', 'dbname'), false, $dboptions);
} catch (\moodle_exception $e) {
    echo $OUTPUT->notification('Cannot connect to the database.', 'notifyproblem');
    $CFG->debug = $olddebug;
    ini_set('display_errors', $olddisplay);
    error_reporting($CFG->debug);
    ob_end_flush();
    echo $OUTPUT->footer();
    die();
}
echo $OUTPUT->notification('Connection made.', 'notifysuccess');
$tables = $db->get_tables();
if (!in_array($dbtable, $tables)) {
    echo $OUTPUT->notification('Cannot find the specified table ' . $dbtable, 'notifyproblem');
    $CFG->debug = $olddebug;
    ini_set('display_errors', $olddisplay);
    error_reporting($CFG->debug);
    ob_end_flush();
    echo $OUTPUT->footer();
    die();
}
echo $OUTPUT->notification('Table ' . $dbtable . ' found.', 'notifysuccess');

$cols = $db->get_columns($dbtable);
if (empty($cols)) {
    echo $OUTPUT->notification('Can not read external table.', 'notifyproblem');
} else {
    $columns = array_keys((array)$cols);
    echo $OUTPUT->notification('External table contains following columns:<br />' . implode(', ', $columns), 'notifysuccess');
}

$db->dispose();

$CFG->debug = $olddebug;
ini_set('display_errors', $olddisplay);
error_reporting($CFG->debug);
ob_end_flush();
echo $OUTPUT->footer();
