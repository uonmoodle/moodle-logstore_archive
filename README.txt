ABOUT
==========
The 'Standard log archive' module was developed by
    Neill Magill

It was heavily based on the core external database logstore developed by
    Petr Skoda

This module may be distributed under the terms of the General Public License
(see http://www.gnu.org/licenses/gpl.txt for details)

PURPOSE
==========
The Standard Log Archive log store is designed to move standard log entries onto
an external database to reduce the space requirements of the main Moodle database
while still allowing access to old log data via the Moodle user interface.

INSTALLATION
==========
The Standard Log Archive log store follows the standard installation procedure.

1. Create folder <path to your moodle dir>/admin/tool/log/store/archive.
2. Extract files from folder inside archive to created folder.
3. Visit page Home ► Site administration ► Notifications to complete installation.
