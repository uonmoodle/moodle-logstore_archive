<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Helper trait config.
 *
 * @package    logstore_archive
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2015 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace logstore_archive\helper;

/**
 * Helper trait to add a function that gets config information for the plugin.
 *
 * @package    logstore_archive
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2015 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
trait config {
    /**
     * Api to get plugin config
     *
     * @param string $name name of the config.
     * @param null|mixed $default default value to return.
     *
     * @return mixed|null return config value.
     */
    protected function get_config($name, $default = null) {
        $value = get_config('logstore_archive', $name);
        if ($value !== false) {
            return $value;
        }
        return $default;
    }
}
