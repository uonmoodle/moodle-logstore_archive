<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Helper trait externaldb.
 *
 * @package    logstore_archive
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2015 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace logstore_archive\helper;

/**
 * Helper trait that connects to an external database,
 * it requires the logstore_archive\helper\config trait is also used.
 *
 * @package    logstore_archive
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2015 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
trait externaldb {
    /** @var \moodle_database $extdb Stores the connection to an external database. */
    protected $extdb;

    /**
     * Setup the Database.
     *
     * @return bool
     */
    protected function init() {
        if (isset($this->extdb)) {
            return !empty($this->extdb);
        }

        $dbdriver = $this->get_config('dbdriver');
        if (empty($dbdriver)) {
            $this->extdb = false;
            return false;
        }
        list($dblibrary, $dbtype) = explode('/', $dbdriver);

        if (!$db = \moodle_database::get_driver_instance($dbtype, $dblibrary, true)) {
            debugging("Unknown driver $dblibrary/$dbtype", DEBUG_DEVELOPER);
            $this->extdb = false;
            return false;
        }

        $dboptions = array();
        $dboptions['dbpersist'] = $this->get_config('dbpersist', '0');
        $dboptions['dbsocket'] = $this->get_config('dbsocket', '');
        $dboptions['dbport'] = $this->get_config('dbport', '');
        $dboptions['dbschema'] = $this->get_config('dbschema', '');
        $dboptions['dbcollation'] = $this->get_config('dbcollation', '');
        try {
            $db->connect($this->get_config('dbhost'), $this->get_config('dbuser'), $this->get_config('dbpass'),
                $this->get_config('dbname'), false, $dboptions);
            $tables = $db->get_tables();
            if (!in_array($this->get_config('dbtable'), $tables)) {
                debugging('Cannot find the specified table', DEBUG_DEVELOPER);
                $this->extdb = false;
                return false;
            }
        } catch (\moodle_exception $e) {
            debugging('Cannot connect to external database: ' . $e->getMessage(), DEBUG_DEVELOPER);
            $this->extdb = false;
            return false;
        }

        $this->extdb = $db;
        return true;
    }

    /**
     * Kills the database connection if it has been set.
     *
     * @return void
     */
    public function dispose() {
        if (isset($this->extdb)) {
            $this->extdb->dispose();
        }
        $this->extdb = null;
    }

    /**
     * Gets the external database connection.
     *
     * @return \moodle_database
     */
    public function get_extdb() {
        if (!$this->init()) {
            return false;
        }
        return $this->extdb;
    }

    /**
     * Get a config value for the store.
     *
     * @param string $name Config name
     * @param mixed $default default value
     * @return mixed config value if set, else the default value.
     */
    public function get_config_value($name, $default = null) {
        return $this->get_config($name, $default);
    }
}
