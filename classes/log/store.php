<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Standard log archive store.
 *
 * @package    logstore_archive
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2015 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace logstore_archive\log;

/**
 * Standard log archive store.
 *
 * Based on the External database store by Petr Skoda {@link http://skodak.org}
 *
 * @package    logstore_archive
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2015 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class store implements \tool_log\log\store, \core\log\sql_reader {
    use \tool_log\helper\store,
        \tool_log\helper\reader,
        \logstore_archive\helper\externaldb;

    /**
     * Construct
     *
     * @param \tool_log\log\manager $manager
     */
    public function __construct(\tool_log\log\manager $manager) {
        $this->helper_setup($manager);
    }

    /**
     * Get an array of events based on the passed on params.
     *
     * @param string $selectwhere select conditions.
     * @param array $params params.
     * @param string $sort sortorder.
     * @param int $limitfrom limit constraints.
     * @param int $limitnum limit constraints.
     *
     * @return array|\core\event\base[] array of events.
     */
    public function get_events_select($selectwhere, array $params, $sort, $limitfrom, $limitnum) {
        if (!$this->init()) {
            return array();
        }

        if (!$dbtable = $this->get_config('dbtable')) {
            return array();
        }

        $sort = self::tweak_sort_by_id($sort);

        $events = array();
        $sort = self::tweak_sort_by_id($sort);
        $records = $this->extdb->get_records_select($dbtable, $selectwhere, $params, $sort, '*', $limitfrom, $limitnum);

        foreach ($records as $data) {
            if ($event = $this->get_log_event($data)) {
                $events[$data->id] = $event;
            }
        }

        return $events;
    }

    /**
     * Fetch records using given criteria returning a Traversable object.
     *
     * Note that the traversable object contains a moodle_recordset, so
     * remember that is important that you call close() once you finish
     * using it.
     *
     * @param string $selectwhere
     * @param array $params
     * @param string $sort
     * @param int $limitfrom
     * @param int $limitnum
     * @return \core\dml\recordset_walk|\core\event\base[]
     */
    public function get_events_select_iterator($selectwhere, array $params, $sort, $limitfrom, $limitnum) {
        if (!$this->init()) {
            return array();
        }
        if (!$dbtable = $this->get_config('dbtable')) {
            return array();
        }
        $sort = self::tweak_sort_by_id($sort);
        $recordset = $this->extdb->get_recordset_select($dbtable, $selectwhere, $params, $sort, '*', $limitfrom, $limitnum);
        return new \core\dml\recordset_walk($recordset, array($this, 'get_log_event'));
    }

    /**
     * Returns an event from the log data.
     *
     * @param type $data
     * @return \core\event\base
     */
    public function get_log_event($data) {
        $extra = array('origin' => $data->origin, 'realuserid' => $data->realuserid, 'ip' => $data->ip);
        $data = (array)$data;
        $id = $data['id'];
        $data['other'] = self::decode_other($data['other']);
        if ($data['other'] === false) {
            $data['other'] = array();
        }
        unset($data['origin']);
        unset($data['ip']);
        unset($data['realuserid']);
        unset($data['id']);

        $event = \core\event\base::restore($data, $extra);
        if (!$event) {
            $event = null;
        }
        return $event;
    }

    /**
     * Get number of events present for the given select clause.
     *
     * @param string $selectwhere select conditions.
     * @param array $params params.
     *
     * @return int Number of events available for the given conditions
     */
    public function get_events_select_count($selectwhere, array $params) {
        if (!$this->init()) {
            return 0;
        }
        if (!$dbtable = $this->get_config('dbtable')) {
            return 0;
        }
        return $this->extdb->count_records_select($dbtable, $selectwhere, $params);
    }

    /**
     * Are the new events appearing in the reader?
     *
     * @return bool true means new log events are being added, false means no new data will be added
     */
    public function is_logging() {
        return false;
    }

    /**
     * @see sql_reader::get_events_select_exists
     */
    public function get_events_select_exists(string $selectwhere, array $params): bool {
        if (!$this->init()) {
            return false;
        }
        if (!$dbtable = $this->get_config('dbtable')) {
            return false;
        }
        return $this->extdb->record_exists_select($dbtable, $selectwhere, $params);
    }
}
