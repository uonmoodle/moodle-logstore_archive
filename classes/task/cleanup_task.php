<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Standard log archive reader cleanup task.
 *
 * @package    logstore_archive
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2015 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace logstore_archive\task;

/**
 * Standard log archive log reader cleanup task.
 * It deletes log entries older than a configurable time period.
 *
 * @package    logstore_archive
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2015 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class cleanup_task extends \core\task\scheduled_task {
    use \logstore_archive\helper\config,
        \logstore_archive\helper\externaldb;

    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name() {
        return get_string('taskcleanup', 'logstore_archive');
    }

    /**
     * Do the job.
     * Throw exceptions on errors (the job will be retried).
     * @return void
     */
    public function execute() {
        if (!$this->init()) {
            mtrace("Database not setup.");
            return;
        }
        $logtable = $this->get_config('dbtable');
        $loglifetime = (int)$this->get_config('archivelifetime');
        if (empty($loglifetime) || $loglifetime < 0) {
            return;
        }
        $logarchiveafter = (int)$this->get_config('archiveafter');

        $loglifetime = time() - ($loglifetime + $logarchiveafter);
        $lifetimep = array($loglifetime);

        $start = time();

        while ($min = $this->extdb->get_field_select($logtable, "MIN(timecreated)", "timecreated < ?", $lifetimep)) {
            // Break this down into chunks to avoid transaction for too long and generally thrashing database.
            // Experiments suggest deleting one day takes up to a few seconds; probably a reasonable chunk size usually.
            // If the cleanup has just been enabled, it might take e.g a month to clean the years of logs.
            $params = array(min($min + 86400, $loglifetime));
            $this->extdb->delete_records_select($logtable, "timecreated < ?", $params);
            if (time() > $start + 300) {
                // Do not churn on log deletion for too long each run.
                break;
            }
        }

        mtrace(" Deleted old log records from standard archive store.");
    }
}
