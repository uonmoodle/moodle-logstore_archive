<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace logstore_archive\local\testing;

/**
 * Sets up a table in the Moodle database for running unit tests.
 *
 * @package    logstore_archive
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2018 University of Nottingham
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
trait local_table {
    /** @var string The name of the table created for the tests that the logstore should be configured to use. */
    protected static $tablename;

    /** @var string The path to the table definition file. */
    protected static $tablefile = '/admin/tool/log/store/archive/tests/fixtures/database.xml';

    /**
     * Create a table for the plugin and ensure the database connection is setup for the test.
     *
     * @global \moodle_database $DB
     */
    public function setUp(): void {
        global $CFG, $DB;
        parent::setUp();
        // Create a table for the logstore to use.
        $dbman = $DB->get_manager();
        $dbman->install_from_xmldb_file($CFG->dirroot . static::$tablefile);
        self::$tablename = 'logstore_archive_log';
        // Setup the plugin to point to the table.
        $parts = explode('_', get_class($DB));
        set_config('dbdriver', $parts[1] . '/' . $parts[0], 'logstore_archive');
        set_config('dbhost', $CFG->dbhost, 'logstore_archive');
        set_config('dbuser', $CFG->dbuser, 'logstore_archive');
        set_config('dbpass', $CFG->dbpass, 'logstore_archive');
        set_config('dbname', $CFG->dbname, 'logstore_archive');
        set_config('dbtable', $CFG->prefix . self::$tablename, 'logstore_archive');
        if (!empty($CFG->dboptions['dbpersist'])) {
            set_config('dbpersist', 1, 'logstore_archive');
        } else {
            set_config('dbpersist', 0, 'logstore_archive');
        }
        if (!empty($CFG->dboptions['dbsocket'])) {
            set_config('dbsocket', $CFG->dboptions['dbsocket'], 'logstore_archive');
        } else {
            set_config('dbsocket', '', 'logstore_archive');
        }
        if (!empty($CFG->dboptions['dbport'])) {
            set_config('dbport', $CFG->dboptions['dbport'], 'logstore_archive');
        } else {
            set_config('dbport', '', 'logstore_archive');
        }
        if (!empty($CFG->dboptions['dbschema'])) {
            set_config('dbschema', $CFG->dboptions['dbschema'], 'logstore_archive');
        } else {
            set_config('dbschema', '', 'logstore_archive');
        }
        if (!empty($CFG->dboptions['dbcollation'])) {
            set_config('dbcollation', $CFG->dboptions['dbcollation'], 'logstore_archive');
        } else {
            set_config('dbcollation', '', 'logstore_archive');
        }
    }

    /**
     * Destroy the table for the plugin.
     *
     * @global \moodle_database $DB
     */
    public function tearDown(): void {
        global $CFG, $DB;
        $dbman = $DB->get_manager();
        $dbman->delete_tables_from_xmldb_file($CFG->dirroot . static::$tablefile);
        self::$tablename = null;
        parent::tearDown();
    }
}
